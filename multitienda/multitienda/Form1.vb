﻿Imports MySql.Data.MySqlClient
Imports MySql.Data
Imports System.Configuration
Public Class Form1
    Dim cnn As New MySqlConnection("Database=multitienda;Data Source=localhost;User Id=root;password=majora")
    Dim cmd As New MySqlCommand
    Dim da As New MySqlDataAdapter
    Dim dt As New DataTable
    Dim ds As New DataSet
    Dim dr As DataRow
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cnn.Open()

        dt = New DataTable
        da = New MySqlDataAdapter("SELECT nom_articulo,precio_articulo,total_articulos FROM cat_articulos", cnn)
        da.Fill(dt)
        dgvArt.DataSource = dt
        cnn.Close()
    End Sub

    Private Sub btGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btGuardar.Click
        cnn.Open()

        cmd = New MySqlCommand("INSERT INTO cat_articulos(nom_articulo,precio_articulo,total_articulos) VALUES ('" + tbNombre.Text + "','" + tbPrecio.Text + "','" + tbtotal.Text + "')", cnn)
        cmd.ExecuteNonQuery()
        MessageBox.Show("guardado correcto")
        dt = New DataTable
        da = New MySqlDataAdapter("SELECT nom_articulo,precio_articulo,total_articulos FROM cat_articulos", cnn)
        da.Fill(dt)
        dgvArt.DataSource = dt

        cnn.Close()

    End Sub

    Private Sub btCambio_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btCambio.Click
        cnn.Open()

        cmd = New MySqlCommand("UPDATE cat_articulos SET precio_articulo='" + tbPrecio.Text + "',total_articulos='" + tbtotal.Text + "' WHERE nom_articulo='" + tbNombre.Text + "'", cnn)
        cmd.ExecuteNonQuery()
        MessageBox.Show("Cambio correcto")
        dt = New DataTable
        da = New MySqlDataAdapter("SELECT nom_articulo,precio_articulo,total_articulos FROM cat_articulos", cnn)
        da.Fill(dt)
        dgvArt.DataSource = dt

        cnn.Close()

    End Sub

    Private Sub Borrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Borrar.Click
        cnn.Open()

        cmd = New MySqlCommand("DELETE FROM cat_articulos WHERE nom_articulo='" + tbNombre.Text + "' ", cnn)
        cmd.ExecuteNonQuery()
        MessageBox.Show("Se borro correctamente")
        dt = New DataTable
        da = New MySqlDataAdapter("SELECT nom_articulo,precio_articulo,total_articulos FROM cat_articulos", cnn)
        da.Fill(dt)
        dgvArt.DataSource = dt

        cnn.Close()
    End Sub

 
End Class

