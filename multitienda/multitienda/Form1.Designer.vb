﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tbNombre = New System.Windows.Forms.TextBox()
        Me.tbPrecio = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.tbtotal = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dgvArt = New System.Windows.Forms.DataGridView()
        Me.btGuardar = New System.Windows.Forms.Button()
        Me.btCambio = New System.Windows.Forms.Button()
        Me.Borrar = New System.Windows.Forms.Button()
        CType(Me.dgvArt, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(98, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Nombre del articulo"
        '
        'tbNombre
        '
        Me.tbNombre.Location = New System.Drawing.Point(117, 12)
        Me.tbNombre.Name = "tbNombre"
        Me.tbNombre.Size = New System.Drawing.Size(186, 20)
        Me.tbNombre.TabIndex = 1
        '
        'tbPrecio
        '
        Me.tbPrecio.Location = New System.Drawing.Point(207, 41)
        Me.tbPrecio.Name = "tbPrecio"
        Me.tbPrecio.Size = New System.Drawing.Size(96, 20)
        Me.tbPrecio.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(13, 44)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(91, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Precio del articulo"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'tbtotal
        '
        Me.tbtotal.Location = New System.Drawing.Point(207, 70)
        Me.tbtotal.Name = "tbtotal"
        Me.tbtotal.Size = New System.Drawing.Size(96, 20)
        Me.tbtotal.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(13, 70)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(84, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "total de articulos"
        '
        'dgvArt
        '
        Me.dgvArt.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvArt.Location = New System.Drawing.Point(12, 99)
        Me.dgvArt.Name = "dgvArt"
        Me.dgvArt.Size = New System.Drawing.Size(420, 150)
        Me.dgvArt.TabIndex = 6
        '
        'btGuardar
        '
        Me.btGuardar.Location = New System.Drawing.Point(357, 13)
        Me.btGuardar.Name = "btGuardar"
        Me.btGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btGuardar.TabIndex = 7
        Me.btGuardar.Text = "Guardar"
        Me.btGuardar.UseVisualStyleBackColor = True
        '
        'btCambio
        '
        Me.btCambio.Location = New System.Drawing.Point(357, 46)
        Me.btCambio.Name = "btCambio"
        Me.btCambio.Size = New System.Drawing.Size(75, 23)
        Me.btCambio.TabIndex = 8
        Me.btCambio.Text = "Cambio"
        Me.btCambio.UseVisualStyleBackColor = True
        '
        'Borrar
        '
        Me.Borrar.Location = New System.Drawing.Point(357, 75)
        Me.Borrar.Name = "Borrar"
        Me.Borrar.Size = New System.Drawing.Size(75, 23)
        Me.Borrar.TabIndex = 9
        Me.Borrar.Text = "Borrar"
        Me.Borrar.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(444, 261)
        Me.Controls.Add(Me.Borrar)
        Me.Controls.Add(Me.btCambio)
        Me.Controls.Add(Me.btGuardar)
        Me.Controls.Add(Me.dgvArt)
        Me.Controls.Add(Me.tbtotal)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.tbPrecio)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.tbNombre)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Form1"
        Me.Text = "Articulo"
        CType(Me.dgvArt, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tbNombre As System.Windows.Forms.TextBox
    Friend WithEvents tbPrecio As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tbtotal As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dgvArt As System.Windows.Forms.DataGridView
    Friend WithEvents btGuardar As System.Windows.Forms.Button
    Friend WithEvents btCambio As System.Windows.Forms.Button
    Friend WithEvents Borrar As System.Windows.Forms.Button

End Class
